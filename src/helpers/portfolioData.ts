const portfolioData = [
	{
		title: 'Some text',
		src: './images/cards/card1.jpg',
		type: 'code',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis.',
	},
	{
		title: 'Some text',
		src: './images/cards/card1.jpg',
		type: 'code',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis.',
	},
	{
		title: 'Some text',
		src: './images/cards/card2.png',
		type: 'ui',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis.',
	},
	{
		title: 'Some text',
		src: './images/cards/card2.png',
		type: 'ui',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis.',
	},
];

export default portfolioData;
