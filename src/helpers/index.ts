import boxData from './boxData';
import expertiseData from './expertiseData';
import feedbackData from './feedbackData';
import educationData from './educationData';
import photoData from './photoData';
export { boxData, expertiseData, feedbackData, educationData, photoData };
