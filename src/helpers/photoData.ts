const photoData = {
	name: 'John Doe',
	title: 'Programmer. Creative. Innovator',
	description:
		'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque',
	avatar: '/images/peoples/d1.jpeg',
};

export default photoData;
