import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import InnerPage from './pages/InnerPage/InnerPage';
import StartPage from './pages/StartPage/StartPage';
import startMirage from './services/index.js';
import store from './redux';
import { Provider } from 'react-redux';

const router = createBrowserRouter([
	{
		path: '/main',
		element: <InnerPage />,
	},
	{
		path: '/',
		element: <StartPage />,
	},
]);
startMirage();

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<Provider store={store}>
		<RouterProvider router={router} />
	</Provider>
);
