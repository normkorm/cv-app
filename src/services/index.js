import { createServer, Model } from 'miragejs';
import { educationData } from '../helpers';
export default function startMirage() {
	let storageLocal = JSON.parse(localStorage.getItem('skills')) || [];

	createServer({
		models: {
			education: Model,
			skill: Model,
		},
		seeds(server) {
			educationData.map((elem) => {
				server.create('education', elem);
			});
			storageLocal.map((elem) => {
				server.create('skill', { skill: elem });
			});
		},
		routes() {
			this.timing = 3000;
			this.get('api/educations', (schema) => {
				return schema.educations.all();
			});
			this.get('api/skills', (schema) => {
				return schema.skills.all();
			});
			this.post('api/skills', (schema, request) => {
				const newSkill = JSON.parse(request.requestBody);
				storageLocal.push(newSkill);
				localStorage.setItem('skills', JSON.stringify(storageLocal));
				return schema.skills.create(newSkill);
			});
		},
	});
}
