import React, { useState } from 'react';
import styles from './InnerPage.module.scss';
import AboutMe from '../../components/AboutMe/AboutMe';
import Education from '../../components/Education/Education';
import Experience from '../../components/Experience/Experience';
import Contacts from '../../components/Contacts/Contacts';
import Feedbacks from '../../components/Feedbacks/Feedbacks';
import Panel from '../../components/Panel/Panel';
import Portfolio from '../../components/Portfolio/Portfolio';

import { boxData, expertiseData, feedbackData } from '../../helpers';
import Skills from '../../components/Skills/Skills';
const InnerPage = () => {
	const [showPanel, setShowPanel] = useState(false);
	return (
		<>
			<Panel showPanel={showPanel} setShowPanel={setShowPanel} />
			<main className={`${styles.main} ${showPanel && styles.main_active}`}>
				<AboutMe data={boxData} />
				<Education />
				<Skills />
				<Experience data={expertiseData} />
				<Portfolio key={`portfolio-${showPanel}`} />
				<Contacts />
				<Feedbacks data={feedbackData} />
			</main>
		</>
	);
};

export default InnerPage;
