import React from 'react';
import PhotoBox from '../../components/PhotoBox/PhotoBox';
import styles from './StartPage.module.css';
import { photoData } from '../../helpers';
const StartPage = () => {
	return (
		<main className={styles.main}>
			<PhotoBox data={photoData} isBig={true} />
		</main>
	);
};

export default StartPage;
