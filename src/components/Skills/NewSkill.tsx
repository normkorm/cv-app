import React from 'react';
import styles from './NewSkill.module.scss';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { addNewSkill } from '../../redux/skills';
import { useAppDispatch } from '../../redux/hooks';

const NewSkill = () => {
	const dispatch = useAppDispatch();
	const formik = useFormik({
		initialValues: {
			skill: '',
			level: '',
		},
		validationSchema: Yup.object({
			skill: Yup.string()
				.min(2, 'Skill name must be at least 2 characters')
				.max(15, 'Skill name must not exceed 15 characters')
				.required('Skill name is a required field'),
			level: Yup.number()
				.min(10, 'Skill range must be greater than or equal to 10')
				.max(100, 'Skill range must be less than or equal to 100')
				.required('Skill level is a required field')
				.typeError('Skill level must be a number'),
		}),
		onSubmit: async (values) => {
			const new_value = {
				skill: values.skill,
				level: {
					width: `${values.level}%`,
				},
			};
			dispatch(addNewSkill(new_value));
		},
	});

	const skillError = formik.touched.skill && formik.errors.skill;
	const levelError = formik.touched.level && formik.errors.level;
	const allError = skillError === undefined && levelError === undefined;

	return (
		<form onSubmit={formik.handleSubmit} className={styles.form}>
			<label className={styles.label}>
				<p>Skill name:</p>
				<input
					type='text'
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					value={formik.values.skill}
					name='skill'
					placeholder='Enter skill name'
					className={skillError && styles.errorInput}
				/>
			</label>
			{skillError && <p className={styles.errorText}>{formik.errors.skill}</p>}
			<label className={styles.label}>
				<p>Skill range:</p>
				<input
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					value={formik.values.level}
					name='level'
					placeholder='Enter skill range'
					className={levelError && styles.errorInput}
				/>
			</label>
			{levelError && <p className={styles.errorText}>{formik.errors.level}</p>}
			<button
				type='submit'
				disabled={!allError}
				className={!allError && styles.disabled}
			>
				Add skill
			</button>
		</form>
	);
};

export default NewSkill;
