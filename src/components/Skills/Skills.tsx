import React, { useEffect, useState } from 'react';
import Line from './Line';
import styles from './Skills.module.scss';
import Button from '../Button/Button';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import NewSkill from './NewSkill';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { getAllSkills } from '../../redux/skills';

const Skills = () => {
	const dispatch = useAppDispatch();
	const skills = useAppSelector((state) => state.skills);
	const { data, status } = skills;
	const [open, setOpen] = useState(false);

	useEffect(() => {
		dispatch(getAllSkills());
	}, [dispatch]);

	return (
		<section id='skills'>
			<div className={styles.top}>
				<h2>Skills</h2>
				<Button
					text='Open edit'
					icon={faEdit}
					onClick={() => setOpen((prev) => !prev)}
				/>
			</div>
			{open && <NewSkill />}
			{data?.map((elem) => (
				<div style={elem.skill.level} className={styles.level}>
					{elem.skill.skill}
				</div>
			))}
			{status !== 'success' && <div>Loading...</div>}
			<Line />
		</section>
	);
};

export default Skills;
