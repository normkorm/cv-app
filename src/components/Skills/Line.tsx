import React from 'react';
import styles from './Line.module.scss';

const levels = ['Beginner', 'Proficient', 'Expert', 'Master'];

const Line = () => {
	return (
		<div className={styles.container}>
			{levels.map((elem) => (
				<div className={styles.line}>
					<span>{elem}</span>
				</div>
			))}
		</div>
	);
};

export default Line;
