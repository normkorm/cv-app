import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const NavigationCard = (props) => {
	const { link, icon, text } = props.elem;
	return (
		<li>
			<a href={link}>
				<FontAwesomeIcon icon={icon} />
				<p>{text}</p>
			</a>
		</li>
	);
};

export default NavigationCard;
