import React from 'react';
import {
	faUser,
	faGraduationCap,
	faPen,
	faGem,
	faSuitcase,
	faContactCard,
	faComment,
} from '@fortawesome/free-solid-svg-icons';
import styles from './Navigation.module.scss';
import NavigationCard from './NavigationCard';

const navigationData = [
	{
		link: '#box',
		icon: faUser,
		text: 'About me',
	},
	{
		link: '#education',
		icon: faGraduationCap,
		text: 'Education',
	},
	{
		link: '#experience',
		icon: faPen,
		text: 'Experience',
	},
	{
		link: '#skills',
		icon: faGem,
		text: 'Skills',
	},
	{
		link: '#portfolio',
		icon: faSuitcase,
		text: 'Portfolio',
	},
	{
		link: '#contacts',
		icon: faContactCard,
		text: 'Contacts',
	},
	{
		link: '#feedback',
		icon: faComment,
		text: 'feedback',
	},
];

const Navigation = () => {
	return (
		<ul className={styles.ul}>
			{navigationData.map((elem, id) => {
				return <NavigationCard elem={elem} key={id} />;
			})}
		</ul>
	);
};

export default Navigation;
