import React from 'react';
import ExperienceCard from './ExperienceCard';

const Experience = ({ data }) => {
	return (
		<section id='expertise'>
			<h2>Experience</h2>
			<ul>
				{data.map((elem, id) => (
					<ExperienceCard elem={elem} key={id} />
				))}
			</ul>
		</section>
	);
};
export default Experience;
