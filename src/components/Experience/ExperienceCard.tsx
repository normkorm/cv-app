import React from 'react';
import styles from './ExperienceCard.module.scss';

const ExperienceCard = ({ elem }) => {
	return (
		<li className={styles.li}>
			<div className={styles.divLeft}>
				<b>{elem.info.company}</b>
				<p className={styles.date}>{elem.date}</p>
			</div>
			<div>
				<b>{elem.info.job}</b>
				<p>{elem.info.description}</p>
			</div>
		</li>
	);
};

export default ExperienceCard;
