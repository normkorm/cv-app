import React, { useEffect } from 'react';
import styles from './Education.module.scss';
import EducationCard from './EducationCard';
import { getAllEducation } from '../../redux/educations';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';

const Education = () => {
	const educations = useAppSelector((state) => state.educations);
	const dispatch = useAppDispatch();

	useEffect(() => {
		dispatch(getAllEducation());
	}, [dispatch]);

	if (educations.status === 'pending') {
		return (
			<section id='education'>
				<h2>Education</h2>
				<div className={`${styles.timeline} ${styles.active}`}>
					<FontAwesomeIcon className={styles.icon} icon={faSyncAlt} />
				</div>
			</section>
		);
	}

	if (educations.status === 'failed') {
		return (
			<section id='education'>
				<h2>Education</h2>
				<div className={`${styles.timeline} ${styles.active}`}>
					<p>Something went wrong; please review your server connection!</p>
				</div>
			</section>
		);
	}

	return (
		<section id='education'>
			<h2>Education</h2>
			<div className={styles.timeline}>
				<ul>
					{educations.data.map((elem, id) => (
						<EducationCard elem={elem} key={id} />
					))}
				</ul>
			</div>
		</section>
	);
};

export default Education;
