import React from 'react';
import styles from './EducationCard.module.scss';
const EducationCard = ({ elem }) => {
	return (
		<li>
			<div className={styles.timelineDate}>{elem.date}</div>
			<div className={styles.timelineEvent}>
				<p>
					<b>{elem.title}</b>
				</p>
				<p>{elem.text}</p>
			</div>
		</li>
	);
};

export default EducationCard;
