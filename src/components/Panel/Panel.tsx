import React from 'react';
import styles from './Panel.module.scss';
import Button from '../Button/Button';
import { faBars, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import Navigation from '../Navigation/Navigation';
import { Link } from 'react-router-dom';
import PhotoBox from '../PhotoBox/PhotoBox';
import { photoData } from '../../helpers';
const Panel = ({ showPanel, setShowPanel }) => {
	const togglePanel = () => {
		setShowPanel((prev) => !prev);
	};
	return (
		<nav className={`${styles.navbar} ${showPanel && styles.navbar_active}`}>
			<PhotoBox data={photoData} isBig={false} />
			<Button onClick={togglePanel} icon={faBars} />
			<Navigation />
			<Link to='/'>
				<Button text='Go back' icon={faChevronLeft} />
			</Link>
		</nav>
	);
};

export default Panel;
