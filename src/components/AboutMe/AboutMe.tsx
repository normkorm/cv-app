import React, { FC } from 'react';

interface BoxProps {
	data: {
		title: string;
		content: string;
	};
}

const AboutMe: FC<BoxProps> = ({ data }) => {
	return (
		<section id='box'>
			<h2 className='title'>{data.title}</h2>
			<p>{data.content}</p>
		</section>
	);
};

export default AboutMe;
