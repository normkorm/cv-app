import React from 'react';
import FeedbackCard from './FeedbackCard';

const Feedbacks = ({ data }) => {
	return (
		<section id='feedback'>
			<h2>Feedbacks</h2>
			<ul>
				{data.map((elem, id) => (
					<FeedbackCard elem={elem} key={id} />
				))}
			</ul>
		</section>
	);
};

export default Feedbacks;
