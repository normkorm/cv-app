import React from 'react';
import styles from './FeedbackCard.module.scss';
const FeedbackCard = ({ elem }) => {
	return (
		<li className={styles.card}>
			<p>{elem.feedback}</p>
			<div>
				<img
					className={styles.card__image}
					src={elem.reporter.photoUrl}
					alt='avatar'
				/>
				<p>{`${elem.reporter.name},\u00A0`}</p>
				<p>
					<a href={elem.reporter.citeUrl}>{elem.reporter.citeUrl.slice(12)}</a>
				</p>
			</div>
		</li>
	);
};

export default FeedbackCard;
