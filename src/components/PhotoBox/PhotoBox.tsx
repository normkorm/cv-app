import React, { FC } from 'react';
import styles from './PhotoBox.module.scss';
import Button from '../Button/Button';
import { Link } from 'react-router-dom';

interface PhotoBoxProps {
	data: {
		name: string;
		title?: string;
		description?: string;
		avatar: string;
	};
	isBig: boolean;
	className?: string;
}

const PhotoBox: FC<PhotoBoxProps> = (props) => {
	const { name, title, description, avatar } = props.data;
	return (
		<div className={`${styles.container} ${props.className}`}>
			<img src={avatar} alt='photo' className={props.isBig && styles.big} />
			{props.isBig ? <h1>{name}</h1> : <h3>{name}</h3>}
			{props.isBig && <h3>{title}</h3>}
			{props.isBig && <p>{description}</p>}
			{props.isBig && (
				<Link to='/main'>
					<Button text='Know more' />
				</Link>
			)}
		</div>
	);
};

export default PhotoBox;
