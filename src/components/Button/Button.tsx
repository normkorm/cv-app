import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC } from 'react';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

interface ButtonProps {
	text?: string;
	className?: string;
	icon?: IconProp;
	onClick?: () => void;
}
const Button: FC<ButtonProps> = (props) => {
	return (
		<button onClick={props.onClick} className={props.className}>
			{props.icon && <FontAwesomeIcon icon={props.icon} />}
			<span>{props.text}</span>
		</button>
	);
};

export default Button;
