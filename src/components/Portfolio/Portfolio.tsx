import Isotope from 'isotope-layout/js/isotope';
import React, { useRef } from 'react';
import styles from './Portfolio.module.scss';
import portfolioData from '../../helpers/portfolioData';
const Portfolio = () => {
	const isotope = useRef<Isotope | null>(null);
	const [filterKey, setFilterKey] = React.useState('*');

	React.useEffect(() => {
		isotope.current = new Isotope(`.${styles.filterContainer}`, {
			itemSelector: `.${styles.figure}`,
			layoutMode: 'fitRows',
			fitRows: {
				gutter: 15,
			},
		});

		return () => isotope.current.destroy();
	}, []);

	React.useEffect(() => {
		filterKey === '*'
			? isotope.current.arrange({ filter: `*` })
			: isotope.current.arrange({ filter: `.${filterKey}` });
	}, [filterKey]);

	const handleFilterKeyChange = (key) => () => setFilterKey(key);

	return (
		<section className={styles.section} id='portfolio'>
			<h2>Portfolio</h2>
			<div className={styles.top}>
				<button
					className={filterKey === '*' && styles.green}
					onClick={handleFilterKeyChange('*')}
				>
					All
				</button>
				&nbsp;/&nbsp;
				<button
					className={filterKey === 'code' && styles.green}
					onClick={handleFilterKeyChange('code')}
				>
					Code
				</button>
				&nbsp;/&nbsp;
				<button
					className={filterKey === 'ui' && styles.green}
					onClick={handleFilterKeyChange('ui')}
				>
					UI
				</button>
			</div>
			<div className={styles.filterContainer}>
				{portfolioData.map((elem, id) => (
					<figure key={id} className={`${styles.figure} ${elem.type}`}>
						<img src={elem.src} alt='card' />
						<figcaption>
							<b>{elem.title}</b>
							<p>{elem.text}</p>
							<a href='#'>View source</a>
						</figcaption>
					</figure>
				))}
			</div>
		</section>
	);
};

export default Portfolio;
