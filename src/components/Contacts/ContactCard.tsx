import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styles from './ContactCard.module.scss';

const ContactCard = (props) => {
	const { icon, title, url, href } = props;
	return (
		<li className={styles.li}>
			<FontAwesomeIcon icon={icon} />
			<a href={href}>
				<b>{title}</b>
				{url && <p>{url}</p>}
			</a>
		</li>
	);
};

export default ContactCard;
