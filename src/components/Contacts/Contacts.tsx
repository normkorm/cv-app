import React from 'react';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import {
	faTwitter,
	faFacebookF,
	faSkype,
} from '@fortawesome/free-brands-svg-icons';
import ContactCard from './ContactCard';

const contactsData = [
	{
		icon: faPhone,
		title: '500 342 242',
		href: 'tel:500342242',
	},
	{
		icon: faEnvelope,
		title: 'office@kamsolutions.pl',
		href: 'mailto:office@kamsolutions.pl',
	},
	{
		icon: faTwitter,
		title: 'Twitter',
		url: 'https://twitter.com/wordpress',
		href: 'https://www.twitter.com/facebook',
	},
	{
		icon: faFacebookF,
		title: 'Facebook',
		url: 'https://www.facebook.com',
		href: 'https://www.facebook.com',
	},
	{
		icon: faSkype,
		title: 'Skype',
		url: 'kamsolutions.pl',
		href: 'skype:kamsolutions?userinfo',
	},
];

const Contacts = () => {
	return (
		<section id='contacts'>
			<h2>Contacts</h2>
			<ul>
				{contactsData.map((data) => {
					if (data.url) {
						return (
							<ContactCard
								icon={data.icon}
								title={data.title}
								href={data.href}
								url={data.url}
								key={data.title}
							/>
						);
					}
					return (
						<ContactCard
							icon={data.icon}
							title={data.title}
							href={data.href}
							key={data.title}
						/>
					);
				})}
			</ul>
		</section>
	);
};

export default Contacts;
