import { configureStore } from '@reduxjs/toolkit';
import educationsSlice from './educations';
import skillsSlice from './skills';

const store = configureStore({
	reducer: {
		educations: educationsSlice,
		skills: skillsSlice,
	},
});
export default store;

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
