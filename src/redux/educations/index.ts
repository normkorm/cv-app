import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
	data: [],
	status: '',
};
export const getAllEducation = createAsyncThunk('fetchEducation', async () => {
	try {
		const response = await fetch('api/educations');
		const result = await response.json();
		return result.educations;
	} catch (error) {
		console.error('Error fetching education:', error);
		throw error;
	}
});

const educationsSlice = createSlice({
	name: 'educations',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getAllEducation.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(getAllEducation.fulfilled, (state, action) => {
				state.status = 'successful';
				state.data = action.payload;
			})
			.addCase(getAllEducation.rejected, (state) => {
				state.status = 'failed';
			});
	},
});
export default educationsSlice.reducer;
