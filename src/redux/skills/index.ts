import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
	data: [],
	status: '',
};
export const getAllSkills = createAsyncThunk('getSkills', async () => {
	try {
		const response = await fetch('api/skills');
		const result = await response.json();
		return result.skills;
	} catch (error) {
		console.error('Error fetching skills:', error);
		throw error;
	}
});

export const addNewSkill = createAsyncThunk(
	'skills/addNewSkill',
	async (data: object) => {
		const url = 'api/skills';
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(data),
		};
		try {
			const response = await fetch(url, options);
			return response.json();
		} catch (error) {
			console.error('Error adding new skill:', error);
			throw error;
		}
	}
);

const skillsSlice = createSlice({
	name: 'skills',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getAllSkills.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(getAllSkills.fulfilled, (state, action) => {
				state.status = 'success';
				state.data = action.payload;
			})
			.addCase(getAllSkills.rejected, (state) => {
				state.status = 'failed';
			})
			.addCase(addNewSkill.pending, (state) => {
				state.status = 'pending';
			})
			.addCase(addNewSkill.fulfilled, (state, action) => {
				state.status = 'success';
				state.data.push(action.payload);
			})
			.addCase(addNewSkill.rejected, (state) => {
				state.status = 'failed';
			});
	},
});

export default skillsSlice.reducer;
